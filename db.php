<?php
function connect_database() {
	//konfigurasi database
	$db_user = "gn15c1";
	$db_password = "rUyyALa2THAG";
	$db_name = "gn15c1";
	$db_host = "localhost";
	
	//akan selalu dilakukan, lebih baik jika ditulis dalam sebuah fungsi tersendiri
	$koneksi = mysqli_connect($db_host, $db_user, $db_password, $db_name);
	return $koneksi;
}
function get_user($nim) {
	$koneksi = connect_database();
	$sql = "SELECT * FROM user WHERE nim = ?";
	$stmt = mysqli_prepare($koneksi, $sql);
	mysqli_stmt_bind_param($stmt, "s", $nim);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_store_result($stmt);
	$user = array();
	if(mysqli_stmt_num_rows($stmt)){
		mysqli_stmt_bind_result($stmt, $nim, $nama, $password, $alamat, $phone, $description, $birthday, $image);
		mysqli_stmt_fetch($stmt);

		//ubah ke bentuk array
		$user = array("nim" => $nim, "nama" => $nama, "password" => $password, "alamat" => $alamat, "phone" => $phone, "description" =>$description, "birthday" => $birthday, "image"=> $image);
	}
	mysqli_stmt_close($stmt);
	mysqli_close($koneksi);
	return $user;
}