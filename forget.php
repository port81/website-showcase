<!DOCTYPE html>
<html>
<head>
  <title>Showcase - Forgot</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <div id="center1" class="header">
    <h2>Forget</h2>
  </div>
  <div id="center2" class="box">
  <form action="/" method="post">
    <p>
      <label>Insert Your Email</label>
    </p>
    <div>
      <input class="textbox" type="email" placeholder="user@ti.ukdw.ac.id"/>
    </div>
    <p><a href="token.php" class="btn">Submit</a></p>
  </form>
  </div>
</body>
