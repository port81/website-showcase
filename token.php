<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Showcase - Thanks</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <div class="header">
    <h1>Forgot</h1>
  </div>
  <div>
  <h2>Terima Kasih</h2>
  <p>Silahkan check email anda untuk melihat token yang telah diberikan</p>
  <form action="/" method="post">
    <p>
      <label>Masukkan Token</label>
    </p>
    <div>
      <input class="textbox" type="text"/>
    </div>
    <p><a href="dashboard.html" class="btn">Submit</a>
    <a href="https://www.gmail.com" target="_blank" class="btn">Gmail</a>
    </p>
  </form>
  </div>
</body>
