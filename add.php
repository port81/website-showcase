<?php session_start(); ?>
<!DOCTYPE html>
<html>	
<head>
	<script src="script/script.js"></script> 
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta charset="UTF-8">
	<title>FTI UKDW ShowCase</title>
</head>
<body>
	<?php if(!isset($_SESSION["status"])){
			include"header.php";
		}else {
			include"header_2.php";
		}
		?>

		<h3>Tambah Karya</h3>
		<form action="add_karya.php" method="post" enctype="multipart/form-data">
			<table>
				<tr><td><label>Title : </label></td><td><input type="text" size="60" name="title" value="type title here"></td></tr>
				<tr><td><label>Category : </label></td><td><select name="category">
<option value="" selected="selected"> --- select category --</option>
 <option value="application"> Application</option>
 <option value="music"> Music</option>
 <option value="video"> Video</option>
 <option value="award"> Awards</option>
</select></td></tr>
				<tr><td><label>Deskripsi : </label></td><td><textarea rows="8" cols="60" name="description">Type article here</textarea></td></tr>
				<tr><td><label>Image : </label></td><td><input type="file" name="fotokarya" accept="image/*"></td></tr>
				<tr><td></td><td><input type="submit" name="tambah" value="TAMBAH KARYA"> | <button class="btn" type="button" onclick="window.location='dashboard.php';return false;">Cancel</button>
				</td></tr>
			</table>
		</form>
		<footer>&copy;<samp>UKDW 2016</samp></footer>
	</div>
</body>
</html>