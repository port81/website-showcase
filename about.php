<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<script src="script/script.js"></script> 
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>FTI UKDW ShowCase</title>
</head>
<body>
	<?php if(!isset($_SESSION["status"])){
			include"header.php";
		}else {
			include"header_2.php";
		}
		?>
		<h2>About Us</h2>
		<table >
			<thead>
				<tr>
					<td></td>
					<th>NIM</th>								
					<th>Name</th>
					<th>e-mail</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><img src="img/profil.jpg" width="100" alt=""></td>
					<th scope="row">71110048</th>
					<td>Vincent Anthonio Marco Parera</td>
					<td>vincentamp@ti.ukdw.ac.id</td>
				</tr>
				<tr>
					<td><img src="img/profil.jpg" width="100" alt=""></td>
					<th scope="row">71130110</th>
					<td>Dafi Hirdhananda</td>
					<td>dafi.hirdhananda@ti.ukdw.ac.id</td>
				</tr>
				<tr>
					<td><img src="img/profil.jpg" width="100" alt=""></td>
					<th scope="row">71140012</th>
					<td>Yosef Dwiastanto Nugroho</td>
					<td>yosef.dwiastanto@ti.ukdw.ac.id</td>
				</tr>
				<tr>
					<td><img src="img/profil.jpg" width="100" alt=""></td>
					<th scope="row">71140105</th>
					<td>Ericson Rumuy</td>
					<td>ericson.rumuy@ti.ukdw.ac.id</td>
				</tr>
			</tbody>
		</table>
	</div>
	<footer>&copy;<samp>UKDW 2016</samp></footer>
</body>
</html>