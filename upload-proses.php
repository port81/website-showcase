<?php

	$target_dir = "img/";
	$foto = $_FILES["foto"];
	$target_file = $target_dir . basename($foto["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	
	if(isset($_POST["submit"])) {
	    $check = getimagesize($foto["tmp_name"]);
	    if($check !== false) {
	    	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    			echo "Sorry, hanya bisa JPG, JPEG, PNG & GIF.<br>";
    			$uploadOk = 0;
			} else {
				echo "File adalah gambar - " . $check["mime"] . ".<br>";
				if (move_uploaded_file($foto["tmp_name"], $target_file)) {
        			echo "File ". basename($foto["name"]). " sudah berhasil diupload!";
        			echo "<br><img src='img/".$foto["name"]."' alt='foto'></img>";
    			} else {
        			echo "Sorry, upload gagal.";
    			}
				$uploadOk = 1;
			}		
	    } else {
	        echo "File bukan gambar.";
	        $uploadOk = 0;
	    }
	}

?>